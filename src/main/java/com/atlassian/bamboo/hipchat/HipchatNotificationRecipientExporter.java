package com.atlassian.bamboo.hipchat;

import com.atlassian.bamboo.notification.NotificationRecipientExporter;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.notification.NotificationRecipientProperties;
import com.atlassian.bamboo.specs.builders.notification.HipChatRecipient;
import com.atlassian.bamboo.specs.model.notification.HipChatRecipientProperties;
import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.util.Narrow;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class HipchatNotificationRecipientExporter implements NotificationRecipientExporter {
    private static final Logger log = Logger.getLogger(HipchatNotificationRecipientExporter.class);

    @NotNull
    @Override
    public NotificationRecipient toSpecsEntity(@NotNull String recipientKey, @NotNull String recipient) {
        HipchatRecipientConfiguration config = HipchatRecipientConfiguration.parse(recipient);
        return new HipChatRecipient()
                .apiToken(config.getApiToken())
                .room(config.getRoom())
                .notifyUsers(config.isNotify());

    }

    @NotNull
    @Override
    public String importNotificationRecipient(NotificationRecipientProperties recipient) {
        final HipChatRecipientProperties hipChatRecipient = Narrow.downTo(recipient, HipChatRecipientProperties.class);
        if (hipChatRecipient != null) {
            return hipChatRecipient.getApiToken() + '|' + String.valueOf(hipChatRecipient.isNotifyUsers()) + '|' + hipChatRecipient.getRoom();
        }
        throw new IllegalStateException("Don't know how to import notification type class: " + recipient.getClass().getName());
    }

}
